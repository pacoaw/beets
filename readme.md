# Beets
Personal configuration for beet

## Links
- Website http://beets.radbox.org
- Documentation http://beets.readthedocs.org/en/latest/index.html
- Code https://github.com/sampsyo/beets/

## Setup
1. Install python https://www.python.org/downloads/windows
2. Install pip by `python get-pip.py` from https://bootstrap.pypa.io/get-pip.py
3. Install beets `pip install beets`
4. Configure by moving config.yaml to `/Users/{You}/AppData/Local/Roaming/config.yaml`
5. Correct the path to python in the .reg file and install the file to enable windows explorer integration